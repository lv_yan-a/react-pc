/**
 * @file RTK数据管理
 * @author ly
 * @createDate 2022年12月11日
 */
import React, { useEffect } from 'react';
import { shallowEqual } from 'react-redux';
// 引入相关的hooks
import { useAppDispatch, useAppSelector } from '@/store';
// 引入对应的方法
import { GET_LIST, getMovieData, SET_LIST } from '@/store/reducers/log';
import Icard from '@/antdComponents/iCard';
import Ibutton from '@/antdComponents/iButton';

/**
 * useSelector默认比较是严格比较（===）
 * 使用react-redux提供的shallowEqual函数作为useSelector的第二参数传递，会对useSelector上一次返回的数据浅比较
 */

const Ireduxtoolkit = () => {
	// 通过useDispatch 派发事件
	const dispatch = useAppDispatch();
	useEffect(() => {
		dispatch(getMovieData());
	}, []);

	const list = useAppSelector(GET_LIST, shallowEqual);
	console.log('rendering');

	const onReadux = () => {
		dispatch(SET_LIST([]));
	};
	return (
		<Icard>
			<Ibutton onClick={onReadux} name={'readux 赋值'}></Ibutton>
			{list.map((item) => {
				return <div key={item.name}>{item.name}</div>;
			})}
		</Icard>
	);
};

export default Ireduxtoolkit;
