/**
 * @file 模板
 * @author 姓名
 * @createDate
 */
import React, { FC, ReactPortal, useEffect, useImperativeHandle, useMemo, useRef, useState, useSyncExternalStore } from 'react';
import todosStore from './store';
import Ibutton from '@/antdComponents/iButton';
import { forwardRefFunc } from '@/hoc/forwardRefHoc';
import { createPortal } from 'react-dom';
import { useSearchParams } from 'react-router-dom';

// 球大小
const ballWidth = 50;
const ballHeight = 50;

// 球运行速度
const moveSpeed = 1;
const timeInterval = 10;

type Stop = () => void;
type Start = () => void;
type BallRef = {
	onStop: Stop;
	onStart: Start;
};
// #----------- 上: ts类型定义 ----------- 分割线 ----------- 下: JS代码 -----------

const DataSharingEnvironment = () => {
	const todos = useSyncExternalStore(todosStore.subscribe, todosStore.getSnapshot);
	const [SearchParams] = useSearchParams();
	const a = SearchParams.get('a');

	const onStop = () => {
		ref.current?.onStop();
	};
	const onStart = () => {
		ref.current?.onStart();
	};

	const clearBall = () => {
		todosStore.updateLocalStorage('position', '');

		onStop();
		setCom(null);
	};

	useEffect(() => {
		if (a === '2') {
			if (todos && JSON.parse(todos)) {
				if (containerRef.current) {
					setCom(createPortal(<Ball2 containerRef={containerRef}></Ball2>, containerRef.current));
				}
			} else {
				setCom(null);
			}
		}

		if (a === '1') {
			if (todos && JSON.parse(todos)) {
				createBall();
			}
		}
	}, []);

	const containerRef = useRef<HTMLDivElement>(null);
	const [com, setCom] = useState<ReactPortal | null>(null);

	const ref = useRef<BallRef>(null);
	const createBall = () => {
		todosStore.updateLocalStorage('position', JSON.stringify({ x: 0, y: 0 }));
		if (containerRef.current) {
			setCom(createPortal(<Ball containerRef={containerRef} ref={ref}></Ball>, containerRef.current));
		}
	};
	return (
		<div>
			<>
				{a === '1' ? (
					<>
						<Ibutton onClick={createBall} name="初始化小球" disabled={!!com}></Ibutton>
						<Ibutton onClick={clearBall} name="删除小球" disabled={!com}></Ibutton>
						<Ibutton onClick={onStop} name="停止"></Ibutton>

						<Ibutton onClick={onStart} name="开始" disabled={!com}></Ibutton>
					</>
				) : null}
			</>

			{com}
			<div
				ref={containerRef}
				style={{
					position: 'relative',
					width: '500px',
					height: '500px',
					backgroundColor: '#f0f0f0',
					overflow: 'hidden'
				}}></div>
		</div>
	);
};

type BallProps = {
	containerRef: React.RefObject<HTMLDivElement>;
};

const Ball = forwardRefFunc<BallRef, BallProps>((props, ref) => {
	const { containerRef } = props;

	useImperativeHandle(ref, () => {
		return {
			onStart,
			onStop
		};
	});

	const todos = useSyncExternalStore(todosStore.subscribe, todosStore.getSnapshot);

	const position = useRef<{ x: number; y: number }>(getLocalstrorage(todos) || (todosStore.initialValue as { x: number; y: number }));

	// 确保 countRef 总是指向最新的 count 值
	useEffect(() => {
		position.current = todos ? (JSON.parse(todos) ? JSON.parse(todos) : todosStore.initialValue) : todosStore.initialValue;
	}, [todos]);

	let moveWdithSpeed = moveSpeed;
	let moveHeightSpeed = moveSpeed;
	// 运动方向
	const directionX = useRef('right');
	const directionY = useRef('bottom');

	// const containerRef = useRef<HTMLDivElement>(null);

	// useEffect(() => {
	const moveBall = () => {
		let nextPosition = {
			x: position.current.x,
			y: position.current.y
		};

		// left边界
		if (nextPosition.x <= 0 && directionX.current === 'left') {
			directionX.current = 'right';
		}

		// top边界
		if (nextPosition.y <= 0 && directionY.current === 'top') {
			directionY.current = 'bottom';
		}

		// right边界
		if (nextPosition.x + ballWidth >= (containerRef.current?.offsetWidth || 0) * 2 && directionX.current === 'right') {
			directionX.current = 'left';
		}

		// bottom边界
		if (nextPosition.y + ballHeight >= (containerRef.current?.offsetHeight || 0) && directionY.current === 'bottom') {
			directionY.current = 'top';
		}

		// 碰到边界时反弹
		if (directionY.current === 'top') {
			moveHeightSpeed = -moveSpeed;
		} else {
			moveHeightSpeed = moveSpeed;
		}

		if (directionX.current === 'left') {
			moveWdithSpeed = -moveSpeed;
		} else {
			moveWdithSpeed = moveSpeed;
		}

		nextPosition.x += moveWdithSpeed;
		nextPosition.y += moveHeightSpeed;

		todosStore.updateLocalStorage('position', JSON.stringify(nextPosition));
	};

	const intervalId = useRef<NodeJS.Timeout | string | number | undefined>(undefined);

	const onStart = () => {
		clearInterval(intervalId.current);
		intervalId.current = setInterval(moveBall, timeInterval);
	};

	const onStop = () => {
		clearInterval(intervalId.current);
	};

	useEffect(() => {
		return () => {
			clearInterval(intervalId.current);
		};
	}, []);

	return (
		<div
			style={{
				position: 'absolute',
				left: position.current.x,
				top: position.current.y,
				width: ballWidth,
				height: ballHeight,
				backgroundColor: 'blue',
				borderRadius: '50%'
			}}
		/>
	);
}, 'ball');

const Ball2: FC<BallProps> = ({ containerRef }) => {
	const todos = useSyncExternalStore(todosStore.subscribe, todosStore.getSnapshot);
	const position = useRef<{ x: number; y: number }>(getLocalstrorage(todos) || (todosStore.initialValue as { x: number; y: number }));

	return (
		<div
			style={{
				position: 'absolute',
				left: position.current.x - (containerRef.current?.offsetHeight || 0),
				top: position.current.y,
				width: ballWidth,
				height: ballHeight,
				backgroundColor: 'blue',
				borderRadius: '50%'
			}}
		/>
	);
};

export default DataSharingEnvironment;

const getLocalstrorage = (todos: string | null) => {
	if (todos && JSON.parse(todos)) return JSON.parse(todos);
	return null;
};
