/**
 * @file 缓存
 * @author ly
 * @createDate 2023年3月31日
 */
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from '../index';

export type PayloadType<T = object> = InitLayoutParams<T>['data'][keyof InitLayoutParams<T>['data']];
export interface InitLayoutParams<T = object> {
	data: {
		[key: string]: {
			key: string;
			value: T;
		};
	};
}
const initialState: InitLayoutParams = {
	data: {}
};

// #----------- 上: ts类型定义 ----------- 分割线 ----------- 下: JS代码 -----------

const keepAlive = createSlice({
	name: 'keepAlive',
	initialState,
	reducers: {
		SET_KEEPALIVE_DATA: (state, { payload, type }: PayloadAction<PayloadType>) => {
			state.data = { ...state.data, [payload.key]: payload };
		}
	}
});

export const GET_KEEPALIVE_DATA = (state: RootState) => state.keepAlive.data;

export const { SET_KEEPALIVE_DATA } = keepAlive.actions;

export default keepAlive.reducer;
