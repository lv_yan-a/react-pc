/**
 * @file 语言切换
 * @author 姓名
 * @createDate
 */
import React, { useMemo } from 'react';
import { Button } from 'antd';
import { useSelector } from 'react-redux';
import { useAppDispatch } from '@/store';
import { GET_LANGUAGE, SET_LANGUAGE } from '@/store/reducers/layout';
import IconFont from '@/utils/iconfont';
import { useTranslation, Trans, Translation } from 'react-i18next';

// #----------- 上: ts类型定义 ----------- 分割线 ----------- 下: JS代码 -----------

const Language = () => {
	const { t, i18n } = useTranslation();

	const dispatch = useAppDispatch();

	const language = useSelector(GET_LANGUAGE);

	const handleSizeChange = () => {
		if (language === 'zh') {
			dispatch(SET_LANGUAGE('en'));
			i18n.changeLanguage('en');
		} else if (language === 'en') {
			dispatch(SET_LANGUAGE('zh'));
			i18n.changeLanguage('zh');
		}
	};

	const themeMenu = [
		{ label: 'icon-zhongyingwenqiehuan-zhongwen', key: 'zh' },
		{ label: 'icon-zhongyingwenqiehuan-yingwen', key: 'en' }
	];

	const languageStr = useMemo(() => themeMenu.find((item) => item.key === language)?.label, [language]);
	return (
		<>
			<Button
				aria-label="ToggleTheme"
				type="text"
				icon={<IconFont type={languageStr || 'icon-zhongyingwenqiehuan-zhongwen'} style={{ transform: 'scale(1.2)' }} />}
				onClick={handleSizeChange}></Button>
		</>
	);
};

export default Language;
