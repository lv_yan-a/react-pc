/**
 * @file 布局
 * @author ly
 * @createDate 2023年6月12日
 */
import React, { useEffect } from 'react';
import { Col, Form, RadioChangeEvent, Row } from 'antd';
import layout from '@/assets/images/layout.jpg';
import layout1 from '@/assets/images/layout1.jpg';
import { useAppSelector, useAppDispatch } from '@/store';
import style from './index.module.scss';
import {
	GET_FOOTER_LAYOUT,
	GET_MENU_LAYOUT,
	GET_TABSMAIN_LAYOUT,
	LayoutMenuType,
	LayoutType,
	SET_FOOTER_LAYOUT,
	SET_LEFT_MENU_COLLAPSED,
	SET_MENU_LAYOUT,
	SET_TABSMAIN_LAYOUT
} from '@/store/reducers/layout';
import Iform, { FormRadioType } from '@/antdComponents/iForm';

const TABSMAIN_LAYOUT = [
	{
		value: 2,
		label: '显示'
	},
	{
		value: 1,
		label: '隐藏'
	}
];

type FormListParam = [FormRadioType, FormRadioType];

type FormParam = { tabsMainLayout: number; footerLayout: number };

// #----------- 上: ts类型定义 ----------- 分割线 ----------- 下: JS代码 -----------

const StyleLayout = () => {
	// 获取菜单初始化数据
	const selectLayout = useAppSelector(GET_MENU_LAYOUT);

	// 获取顶部导航栏初始化数据
	const tabsMainLayout = useAppSelector(GET_TABSMAIN_LAYOUT);

	// 获取底部初始化数据
	const footerLayout = useAppSelector(GET_FOOTER_LAYOUT);

	const dispatch = useAppDispatch();

	const onSelectStyle = (value: LayoutMenuType) => {
		dispatch(SET_MENU_LAYOUT(value));
		dispatch(SET_LEFT_MENU_COLLAPSED(false));
	};

	const getStyle = (value: LayoutMenuType[]) => {
		if (value.indexOf(selectLayout) > -1) {
			return style['select-style'];
		}
		return '';
	};

	const onSelectTabsMainLayout = (e: RadioChangeEvent) => {
		dispatch(SET_TABSMAIN_LAYOUT(e.target.value));
	};

	const onSelectFooterLayout = (e: RadioChangeEvent) => {
		dispatch(SET_FOOTER_LAYOUT(e.target.value));
	};

	const [form] = Form.useForm<FormParam>();

	const formList: FormListParam = [
		{
			type: 'radio',
			key: 0,
			span: 24,
			comConfig: {
				options: TABSMAIN_LAYOUT,
				onChange: onSelectTabsMainLayout
			},
			formItemProps: {
				name: 'tabsMainLayout',
				label: '顶部导航栏',
				labelCol: { span: 6 },
				wrapperCol: { span: 18 }
			}
		},
		{
			type: 'radio',
			key: 1,
			span: 24,
			comConfig: {
				options: TABSMAIN_LAYOUT,
				onChange: onSelectFooterLayout
			},
			formItemProps: { name: 'footerLayout', label: '底部', labelCol: { span: 6 }, wrapperCol: { span: 18 } }
		}
	];

	useEffect(() => {
		form.setFieldsValue({ tabsMainLayout, footerLayout });
	}, []);

	return (
		<div>
			<h2 className="mb-2 text-base">选择布局</h2>
			<Row gutter={32}>
				<Col className={getStyle([1])}>
					<img src={layout} alt="" className={'cursor-pointer '} onClick={() => onSelectStyle(1)} />
				</Col>
				<Col className={getStyle([2, 3])}>
					<img src={layout1} alt="" className={'cursor-pointer '} onClick={() => onSelectStyle(2)} />
				</Col>
			</Row>
			<h2 className="mt-2 mb-2 text-base">组件显示</h2>
			<Iform formProps={{ form: form }} formList={formList}></Iform>
		</div>
	);
};

export default StyleLayout;
