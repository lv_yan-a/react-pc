/**
 * @file 语言切换
 * @author ly
 * @createDate
 */
import React, { useMemo, useState } from 'react';
import enUS from 'antd/locale/en_US';
import zhCN from 'antd/locale/zh_CN';
import dayjs from 'dayjs';
import 'dayjs/locale/zh-cn';
import 'dayjs/locale/en';
import { useAppSelector } from '@/store';
import { GET_LANGUAGE } from '@/store/reducers/layout';

// #----------- 上: ts类型定义 ----------- 分割线 ----------- 下: JS代码 -----------

const useLanguage = () => {
	const language = useAppSelector(GET_LANGUAGE);
	const locale = useMemo(() => {
		if (language === 'zh') {
			dayjs.locale('zh-cn');
			return zhCN;
		} else if (language === 'en') {
			dayjs.locale('en');
			return enUS;
		}
	}, [language]);
	return { locale };
};

export default useLanguage;
